# BattleCity

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.2.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

```
ng build -prod --base-href "./"
```

### Build for Mobile (Cordova)

1. Build with `ng build -op ./www --base-href "./"`
2. Deploy with `cordova run <platform>`

#### First Time Setup - Android

[Official Cordova documentation.](https://cordova.apache.org/docs/en/latest/guide/platforms/android/index.html)

1. Install gradle (e.g. `scoop insatall gradle`; [More on scoop](http://scoop.sh/))
2. You may need to install the appropriate SDKs. (See the Cordova documentation.)
2. `cordova platform add android`
3. Attach your device and test the build process via `cordova run <platform>`.

## Build for Deployment

First, run `npm version patch|minor|major`. This updates the npm and cordova version numbers.

### Web

1. Run `npm run buildWebProd`.
2. Copy files from `dist` directory to server.

### Android

1. Ensure you have battleCity.keystore in your project's root folder.
2. Run `npm run buildCordovaAndroidProd`.
3. Enter password each time prompted.
4. Signed apk file can be found at `./platforms/android/build/outputs/apk/android-release.apk`.
5. Test signed apk via `adb install ./platforms/android/build/outputs/apk/android-release.apk`.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
