export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyAU7SYAXlNkhpi59md-iWHxA9kH2y7mnIk",
    authDomain: "battle-city-4.firebaseapp.com",
    databaseURL: "https://battle-city-4.firebaseio.com",
    projectId: "battle-city-4",
    storageBucket: "battle-city-4.appspot.com",
    messagingSenderId: "671891698931"
  },
  cordovaGooglePlus: {
    clientId: "244926013648-vucq9pfmti9hsg937sm7vdml8m63cach.apps.googleusercontent.com",
    webClientId: "244926013648-66ldbdbebpnr081lh43pgpi4pdgikatu.apps.googleusercontent.com",
  },
};
