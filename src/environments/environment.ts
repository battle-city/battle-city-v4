// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB5aHNx79_IPuQsSIbCYt4lDOVBz4CwwPo",
    authDomain: "battle-city-2.firebaseapp.com",
    databaseURL: "https://battle-city-2.firebaseio.com",
    projectId: "battle-city-2",
    storageBucket: "battle-city-2.appspot.com",
    messagingSenderId: "380793599299"
  },
  cordovaGooglePlus: {
    clientId: "96856222863-gojc9k9s2s1217jv8egmvimerhhv3ajc.apps.googleusercontent.com",
    webClientId: "96856222863-6acuta8906ef5fm6b0p5s2cjmvdrkd3v.apps.googleusercontent.com",
  },
};
