// SEE: https://firebase.google.com/docs/auth/web/manage-users

import { Component, OnInit } from '@angular/core';
// import { User } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { CurrentUserService } from '../services/current-user.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    public currentUserService: CurrentUserService,
    private router: Router,
  ) {
  }

  ngOnInit() {
  }


  login() {
    this.currentUserService.login().then(() => {
      console.log('resolved auth promise. Redirecting to home page.');
      this.router.navigate(['/home']);
    });
  }
}
