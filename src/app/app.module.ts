import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import 'hammerjs';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MomentModule } from 'angular2-moment';

import { RouterModule, PreloadAllModules } from '@angular/router';
import { ROUTES } from './app.routes';

import { AppComponent } from './app.component';

import { HomeComponent } from './home';
import { NavigationComponent } from './navigation/navigation.component';

import { DndModule } from 'ng2-dnd';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { MatchComponent, MatchModule } from './match';
import { SelectComponent } from './match/select/select.component';
import { DetailComponent } from './match/detail/detail.component';
import { ObjNgForPipe } from './obj-ng-for.pipe';
import { ReversePipe } from './reverse.pipe';
import { AddPlayerComponent } from './match/detail/config/add-player/add-player.component';
import { GameComponent } from './match/game/game.component';
import { GamesService } from './services/games.service';
import { ConfigComponent } from './match/detail/config/config.component';
import { MatchViewComponent } from './match/detail/match-view/match-view.component';
import { StateReadablePipe } from './pipes/state-readable.pipe';
import { GameLogModalComponent } from './match/game/game-log-modal/game-log-modal.component';
import { GameEndModalComponent } from './match/game/game-end-modal/game-end-modal.component';
import { LoginComponent } from './login/login.component';
import { CurrentUserService } from './services/current-user.service';
import { ToolsComponent } from './tools/tools.component';
import { DiceModalComponent } from './tools/dice-modal/dice-modal.component';
import { AboutComponent } from './about/about.component';
import { NextGameSnackbarComponent } from './match/game/next-game-snackbar/next-game-snackbar.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavigationComponent,
    MatchComponent,
    SelectComponent,
    DetailComponent,
    ObjNgForPipe,
    ReversePipe,
    AddPlayerComponent,
    GameComponent,
    ConfigComponent,
    MatchViewComponent,
    StateReadablePipe,
    GameLogModalComponent,
    GameEndModalComponent,
    LoginComponent,
    ToolsComponent,
    DiceModalComponent,
    AboutComponent,
    NextGameSnackbarComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    RouterModule.forRoot(ROUTES, { useHash: true, preloadingStrategy: PreloadAllModules }),
    MaterialModule,
    FlexLayoutModule,
    MatchModule,
    DndModule.forRoot(),
    MomentModule,
  ],
  providers: [
    GamesService,
    CurrentUserService,
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    AddPlayerComponent,
    GameLogModalComponent,
    GameEndModalComponent,
    DiceModalComponent,
    NextGameSnackbarComponent,
  ]
})
export class AppModule { }
