import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stateReadable'
})
export class StateReadablePipe implements PipeTransform {

  transform(value: string, args?: any): any {

    switch (value) {
      case 'config': return 'Being Configured';
      case 'inProgress': return 'In Progress';
      case 'done': return 'Done';
      default: return 'Unknown';
    }

  }

}
