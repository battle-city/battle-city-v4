import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MatchComponent } from './';
import { SelectComponent } from './select';
import { DetailComponent } from './detail';
import { GameComponent } from './game';


const matchRoutes: Routes = [
    {
        path: 'match',
        component: MatchComponent,
        children: [
            {
                path: 'select',
                component: SelectComponent,
                children: []
            },
            {
                path: ':matchId',
                component: DetailComponent,
                children: []
            },
            {
                path: ':matchId/game/:gameId',
                component: GameComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(matchRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class MatchModule { }