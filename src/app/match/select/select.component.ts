import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { GAMETYPES } from '../../../gametypes';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css'],
})
export class SelectComponent implements OnInit {

  searchText: '';
  matches: FirebaseListObservable<any[]>;
  tabs;
  selectedTabIndex = 0;
  db: AngularFireDatabase;
  gametypes;

  constructor(db: AngularFireDatabase) {
    this.db = db;
    this.tabs = [
      { label: 'Starting Up', state: 'config' },
      { label: 'In Progress', state: 'inProgress' },
      { label: 'Finished', state: 'done' },
    ];
    this.onTabSelectChange();
    this.gametypes = GAMETYPES;
  }

  ngOnInit() {
  }

  onTabSelectChange() {
    let stateName = this.tabs[this.selectedTabIndex].state;
    this.matches = this.db.list('/matches', {
      query: {
        orderByChild: 'state',
        equalTo: this.tabs[this.selectedTabIndex].state,
      }
    });
  }

}
