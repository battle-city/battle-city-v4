import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GAMETYPES } from 'gametypes';
import * as _ from "lodash";
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import * as firebase from 'firebase';
import { MdDialog, MdDialogRef } from '@angular/material';
import { GamesService } from '../../../services/games.service';
import { Match } from "../../../classes/match";
import { MediaChange, ObservableMedia } from "@angular/flex-layout";


@Component({
  selector: 'app-match-view',
  templateUrl: './match-view.component.html',
  styleUrls: ['./match-view.component.css']
})
export class MatchViewComponent implements OnInit {

  private db: AngularFireDatabase;
  match: Match;
  games: any;
  gamesArray: any[];
  private gamesService;
  gametypes: any[];
  variants: any[];

  constructor(
    private route: ActivatedRoute,
    db: AngularFireDatabase,
    private router: Router,
    gamesService: GamesService,
    public media: ObservableMedia,
  ) {
    this.db = db;
    this.gamesService = gamesService;
  }

  ngOnInit() {

    this.games = {};
    this.gametypes = _.values(GAMETYPES);

    this.route.params.subscribe(params => {
      var matchId = params['matchId'];
      this.match = new Match(matchId, this.db);

      this.match.ref.child('state').on('value', (snapshot) => {
        this.match.state = snapshot.val();
      });

      this.match.ref.child('config').on('value', (snapshot) => {
        this.match.config = snapshot.val();
        this.variants = this.getVariants(this.match.config.gametypeId);
      });

      this.match.ref.child('games').on('value', (snapshot) => {
        this.match.games = _.values(snapshot.val()); // just the IDs.

        // Get info for each game
        _.forEach(this.match.games, (game) => {

          var gameRef = this.gamesService.gamesRef.child(game.id);
          gameRef.on('value', (snapshot) => {
            var game = snapshot.val();
            this.games[game.id] = game;
            this.gamesArray = _.values(this.games);
          });

        });
      });

    });
  }

  getVariants(gametypeId) {
    let gametype = GAMETYPES[gametypeId];
    if (gametypeId && gametype.variants)
      return _.values(GAMETYPES[gametypeId].variants);
    else {
      console.log('The gametype does not have variants.', gametype);
      return null;
    }
  }

}
