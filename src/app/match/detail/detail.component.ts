import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GAMETYPES } from 'gametypes';
import * as _ from "lodash";
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';
import { Match } from '../../classes/match';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  match: Match;
  private db: AngularFireDatabase;

  constructor(
    private route: ActivatedRoute,
    db: AngularFireDatabase,
    private router: Router,
  ) {
    this.db = db;
  }

  ngOnInit() {

    this.route.params.subscribe(params => {
      var matchId = params['matchId'];
      this.match = new Match(matchId, this.db);

      this.match.ref.child('state').on('value', (snapshot) => {
        this.match.state = snapshot.val();
      });

    });

  }

}
