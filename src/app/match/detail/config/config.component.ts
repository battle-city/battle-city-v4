import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GAMETYPES } from 'gametypes';
import * as _ from "lodash";
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import * as firebase from 'firebase';
import { MdDialog, MdDialogRef } from '@angular/material';
import { AddPlayerComponent } from './add-player/add-player.component';
import { GamesService } from '../../../services/games.service';
import { Match } from "../../../classes/match";
import { MediaChange, ObservableMedia } from "@angular/flex-layout";

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.css']
})
export class ConfigComponent implements OnInit {


  matchExists: boolean;
  matchId: string;
  config;
  matchRef: firebase.database.Reference;
  configRef: firebase.database.Reference;
  playersRef: firebase.database.Reference;
  gamesRef: firebase.database.Reference;
  gametype;
  gametypes;
  scoresRef: firebase.database.Reference;
  variants;
  computedGametype; // merge of config.gametype and config.variant.
  computedGametypeScores;
  showSecondaryScores;
  private db: AngularFireDatabase;
  match: Match;

  constructor(
    private route: ActivatedRoute,
    db: AngularFireDatabase,
    private gamesService: GamesService,
    public dialog: MdDialog,
    private router: Router,
    private GamesService: GamesService,
    public media: ObservableMedia,
  ) {
    this.db = db;
    this.config = {};
  }

  ngOnInit() {

    this.gametypes = _.values(GAMETYPES);
    this.showSecondaryScores = false;

    this.route.params.subscribe(params => {
      this.matchId = params['matchId'];
      this.match = new Match(this.matchId, this.db);

      this.configRef = this.match.ref.child('/config');
      this.match.playersRef = this.match.ref.child('/players');
      this.scoresRef = this.configRef.child('scores');
      this.gamesRef = this.GamesService.gamesRef;

      var initialGametype: any = _.values(GAMETYPES)[0];

      this.match.configRef.on('value', (snapshot) => {

        this.config = snapshot.val() || {
          gametypeId: initialGametype.id,
          variantId: initialGametype.variants ? this.getVariants(initialGametype.id)[0] : null,
        };
        this.updateLocalVariants();
        if (snapshot.val() == null)
          this.initializeConfig();
      });

    });

  }

  initializeConfig() {
    this.onChangeGametypeOrVariant(true);
  }

  updateLocalVariants() {
    this.variants = this.getVariants(this.config.gametypeId);
  }

  getVariants(gametypeId) {
    let gametype = GAMETYPES[gametypeId];
    if (gametypeId && gametype.variants)
      return _.values(GAMETYPES[gametypeId].variants);
    else {
      console.log('The gametype does not have variants.', gametype);
      return null;
    }
  }

  onChangeGametypeOrVariant(resetVariantScore: boolean) {

    this.updateLocalVariants();

    let baseGametype = _.cloneDeep(GAMETYPES[this.config.gametypeId]);

    // Reset variant to default. Only do this on gametype change.
    var variantId;
    if (resetVariantScore) {
      variantId = this.variants ? this.variants[0].id : null;
    } else variantId = this.config.variantId;

    let variant = baseGametype.variants ? _.cloneDeep(baseGametype.variants[variantId]) : {};

    _.merge(this.config, baseGametype, variant);
    delete this.config.variants; // We don't want all potential variants to be listed in the db.
    this.config.variantId = variantId; // Because this may need to be null, if there are no variants.

    this.updateLocalVariants(); // convert variants from native object representation to array.

    // Convert this.config.scores to array, but do
    this.config.scores = _.values(_.merge(baseGametype.scores, variant.scores));

    this.configRef.set(this.config);
  }

  setScores() {
    // TODO: Debounce this.
    this.scoresRef.set(this.config.scores);
  }

  promptAddPlayer() {
    let dialogRef = this.dialog.open(AddPlayerComponent);
    dialogRef.componentInstance['match'] = this.match;
  }

  promptShuffleTeams() {
    var teamSize = Number(prompt('Team Size?', '2'));
    if (teamSize && !isNaN(teamSize) && teamSize > 0) this.match.shuffleTeams(teamSize);
    else this.promptShuffleTeams();
  }

  startMatch() {

    this.match.ref.child('state').set('inProgress');

    // create game
    let newGameRef = this.GamesService.createGame(this.matchId, this.match.players, 0, this.config.scores);

    this.flipCoin(newGameRef);

    this.router.navigate(['/match', this.matchId, 'game', newGameRef.key]);
  }

  flipCoin(newGameRef) {
    var chosenTeam = _.sample(this.match.teams);
    var chosenTeamId = chosenTeam[0].team; // get ID from first player in team.

    newGameRef.child('coinFlip').set(chosenTeamId);

    this.gamesService.logEvent(newGameRef.key, 'coinFlip', 'Battle City', this.getTeamExpandedName(chosenTeam) + ' won the coin toss!');

  }

  getTeamExpandedName(team) {
    if (team)
      return 'Team ' + (team[0].team + 1) + ' (' + _.map(team, 'name').join(')(') + ')';
  }

}
