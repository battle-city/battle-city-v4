import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import * as firebase from 'firebase';
import * as _ from "lodash";
import {Match} from "../../../../classes/match";

@Component({
  selector: 'app-add-player',
  templateUrl: './add-player.component.html',
  styleUrls: ['./add-player.component.css']
})
export class AddPlayerComponent implements OnInit {

  db: AngularFireDatabase;
  usersRef: firebase.database.Reference;
  users;
  filteredUsers;
  match: Match; // supplied by detail.component

  constructor(db: AngularFireDatabase) {
    this.db = db;
    this.usersRef = this.db.database.ref('users');
  }

  ngOnInit() {
    this.usersRef.once('value', (snapshot) => {
      this.users = _.values(snapshot.val());
      this.filteredUsers = this.users;
    });
  }

  filterUsers(filterText: string) {
    this.filteredUsers = _.filter(this.users, (user) => {
      return _.includes((user as any).displayName.toLowerCase(), filterText.toLowerCase());
    });
  }

}
