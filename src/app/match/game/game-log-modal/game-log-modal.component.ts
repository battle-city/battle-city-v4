import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-game-log-modal',
  templateUrl: './game-log-modal.component.html',
  styleUrls: ['./game-log-modal.component.css']
})
export class GameLogModalComponent implements OnInit {

  log: any[];

  constructor() { }

  ngOnInit() {
  }

}
