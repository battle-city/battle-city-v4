import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameLogModalComponent } from './game-log-modal.component';

describe('GameLogModalComponent', () => {
  let component: GameLogModalComponent;
  let fixture: ComponentFixture<GameLogModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameLogModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameLogModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
