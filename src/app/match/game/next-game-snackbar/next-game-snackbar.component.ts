import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-next-game-snackbar',
  templateUrl: './next-game-snackbar.component.html',
  styleUrls: ['./next-game-snackbar.component.css']
})
export class NextGameSnackbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public onAction;
  public onDismiss;

}
