import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NextGameSnackbarComponent } from './next-game-snackbar.component';

describe('NextGameSnackbarComponent', () => {
  let component: NextGameSnackbarComponent;
  let fixture: ComponentFixture<NextGameSnackbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NextGameSnackbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NextGameSnackbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
