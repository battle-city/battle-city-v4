import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';
import { ActivatedRoute, Router } from '@angular/router';
import { GAMETYPES } from 'gametypes';
import { GamesService } from '../../services/games.service';
import { MdDialog, MdDialogRef, MdSnackBar } from '@angular/material';
import { GameLogModalComponent } from './game-log-modal/game-log-modal.component';
import { GameEndModalComponent } from './game-end-modal/game-end-modal.component';
import { NextGameSnackbarComponent } from './next-game-snackbar/next-game-snackbar.component';


@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  gameExists: boolean;
  matchId: string;
  gameId: string;
  matchRef: firebase.database.Reference;
  gameRef: firebase.database.Reference;
  game;
  playersRef: firebase.database.Reference;
  players;
  teams;
  showSecondaryScores;
  match;
  activeScore;
  private scoreNames: string[];
  private gamesService;
  private db: AngularFireDatabase;
  scoreAdjustInput: number;
  selectedTeams: any[];
  scores: any[];
  activeScoreIndex: number;
  scoreAdjustOutput: number;

  constructor(
    private route: ActivatedRoute,
    db: AngularFireDatabase,
    private router: Router,
    gamesService: GamesService,
    public dialog: MdDialog,
    public snackBar: MdSnackBar,
  ) {
    this.db = db;
    this.match = {};
    this.game = {};
    this.gamesService = gamesService;
  }

  ngOnInit() {

    this.showSecondaryScores = false;

    this.route.params.subscribe(params => {
      this.gameId = params['gameId'];
      this.matchId = params['matchId'];
      this.matchRef = this.db.database.ref("matches/" + this.matchId);
      this.gameRef = this.gamesService.getGameRef(this.gameId);
      this.playersRef = this.gameRef.child('/players');

      this.matchRef.once('value', (snapshot) => {
        this.match = snapshot.val();

        this.scores = _.values(this.match.config.scores)
        this.activeScore = this.scores[0];
        this.activeScoreIndex = 0;
        this.scoreAdjustInput = this.activeScore.step;
        this.scoreAdjustOutput = this.scoreAdjustInput;
      });

      this.gameRef.once('value', (snapshot) => {
        this.gameExists = snapshot.exists();

        if (!this.gameExists) return;

        this.game = snapshot.val();

        // Handle navigation on game end
        this.gameRef.child('state').on('value', snapshot => {
          this.game.state = snapshot.val();
          if (this.game.state !== 'done')
            return;

          this.matchRef.once('value', snapshot => {
            var match = snapshot.val();
            if (match.state == 'inProgress') {
              let snackBarRef = this.snackBar.openFromComponent(NextGameSnackbarComponent);
              snackBarRef.instance.onDismiss = () => { snackBarRef.dismiss(); };
              snackBarRef.instance.onAction = () => {
                var lastGame: any = _.maxBy(_.values(match.games), 'number');
                this.router.navigate(['/match', this.matchId, 'game', lastGame.id]);
                snackBarRef.dismiss();
              };
            } else if (match.state == 'done') {
              this.router.navigate(['/match', this.matchId]);
            }

          });
        });

        this.playersRef.once('value', (snapshot) => {
          this.players = snapshot.val();
          this.assignScoreRefsToPlayers();
          this.sortPlayersIntoTeams();

          if (this.game.state !== 'done' && this.game.number == 0)
            alert(this.getTeamExpandedName(this.teams[this.game.coinFlip]) + ' won the coin toss!');

        });

        this.gameRef.child('log').orderByChild('timestamp').on('value', (snapshot) => {
          this.game.log = _.flow([_.values, _.reverse])(snapshot.val());
        });

      });

    });

  }

  getTeamExpandedName(team) {
    if (team)
      return 'Team ' + (team[0].team + 1) + ' (' + _.map(team, 'name').join(')(') + ')';
  }

  sortPlayersIntoTeams() {
    this.teams = _.flow([_.groupBy, _.toArray])(this.players, 'team');
  }

  assignScoreRefsToPlayers() {
    _.forEach(this.players, (player) => {
      _.forEach(player.scores, (score) => {
        score.ref = this.playersRef.child(player.userId).child('scores').child(score.id).child('current');
        score.ref.on('value', (snapshot) => {
          score.current = snapshot.val();
        })
      });
    });
  }

  setSelectedTeams() {
    this.selectedTeams = _.filter(this.teams, { selected: true });
  }

  addToScore(changeBy: number) {

    if (!this.selectedTeams || !this.selectedTeams.length)
      return;

    let teamsNumbers: number[] = [];

    _.forEach(this.selectedTeams, (team, index) => {
      teamsNumbers.push(index);
      _.forEach(team, (player) => {
        var activeScore = player.scores[this.activeScore.id];
        var currentScore = parseInt(activeScore.current);
        var newScore = currentScore + changeBy;
        activeScore.ref.set(newScore);
      });
    });

    var message = 'Team' + (this.selectedTeams.length > 1 ? 's ' : ' ') + teamsNumbers.join(', ') + (changeBy > 0 ? ' gained ' : ' lost ') + Math.abs(changeBy) + ' ' + this.activeScore.label;
    this.gamesService.logEvent(this.gameId, changeBy > 0 ? 'addScore' : 'removeScore', '', message);
  }

  cycleActiveScore() {
    this.activeScoreIndex = this.activeScoreIndex + 1;
    if (this.activeScoreIndex >= this.scores.length)
      this.activeScoreIndex = 0;
    this.activeScore = this.scores[this.activeScoreIndex];
  }

  calculateMathExpression(input: string) {

    var pattern = /\(*\d+([\+\-\*\/]\d+)*\)*$/;

    if (!pattern.test(input))
      return "...";

    var amt = eval(input);
    if (isNaN(amt))
      return "...";

    return amt;
  }

  updateScoreAdjustInput(input: string) {
    this.scoreAdjustOutput = this.calculateMathExpression(input);
  }

  showLog() {
    let dialogRef = this.dialog.open(GameLogModalComponent);
    dialogRef.componentInstance['log'] = this.game.log;
  }

  endGame() {
    let dialogRef = this.dialog.open(GameEndModalComponent);
    dialogRef.afterClosed().subscribe(result => {

      if (!result)
        return;

      this.gameRef.child('state').set('done');

      if (result == 'rematch') {
        var newGameRef = this.gamesService.createGame(this.matchId, this.match.players, this.game.number + 1, this.match.config.scores);
        // this.router.navigate(['/match', this.matchId, 'game', newGameRef.key]);
      } else {
        this.matchRef.child('state').set('done');
        // this.router.navigate(['/match', this.matchId]);
      }

    });
  }

}