import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameEndModalComponent } from './game-end-modal.component';

describe('GameEndModalComponent', () => {
  let component: GameEndModalComponent;
  let fixture: ComponentFixture<GameEndModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameEndModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameEndModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
