import { Component, ViewChild } from '@angular/core';
import { MediaChange, ObservableMedia } from "@angular/flex-layout";
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';
import { GAMETYPES } from 'gametypes';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { CurrentUserService } from './services/current-user.service';
import { Player } from './classes/player';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'Battle City';
  matchesRef: firebase.database.Reference;
  @ViewChild('sidenav') sidenav;

  constructor(
    public media: ObservableMedia,
    db: AngularFireDatabase,
    private router: Router,
    public currentUserService: CurrentUserService,
  ) {
    this.matchesRef = db.database.ref('matches');

    // Direct user to login page if they are unauthenticated.
    firebase.auth().onAuthStateChanged(user => {
      if (!currentUserService.user)
        router.navigate(['/login']);
    });


    router.events.subscribe(event => {

      // Keep unauthenticated users on login page.
      if (event instanceof NavigationStart) {
        if (router.routerState.snapshot.url == '/login' && !currentUserService.user) {
          router.navigate(['/login']);
        }
      }

      if (event instanceof NavigationEnd) {
        this.sidenav.close();
      }

    })

  }

  createMatch() {
    let newMatch = {
      creator: this.currentUserService.user.id,
      isPublic: true,
      state: 'config',
    };
    let newMatchRef = this.matchesRef.push(newMatch);
    let player = new Player(0, this.currentUserService.user);
    newMatchRef.child('players').child(this.currentUserService.user.id).set(player);

    this.router.navigate(['/match', newMatchRef.key]);
  }
}
