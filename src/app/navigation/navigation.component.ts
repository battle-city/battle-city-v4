import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }

  menuItems = [
    {
      name: "Dashboard",
      link: './home',
      icon: "dashboard",
      requireLogin: "true"
    },
    {
      name: "Matches",
      link: "./match/select",
      icon: "new_releases",
      requireLogin: "true"
    },
    {
      name: "About",
      link: "about",
      icon: "info_outline"
    }
  ];

}
