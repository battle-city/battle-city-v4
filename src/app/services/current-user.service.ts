import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';
import { environment } from '../../environments/environment';
import { User } from '../classes/user';

declare var cordova;

@Injectable()
export class CurrentUserService {

  public user: User;
  public authFinished: boolean;

  private provider: firebase.auth.GoogleAuthProvider;
  private cordovaGooglePlus;
  private isCordova: boolean;
  private authReturnPromiseResolve;
  private authReturnPromise;
  private cordovaGooglePlusLoginOptions;

  constructor(
    private db: AngularFireDatabase,
  ) {
    this.provider = new firebase.auth.GoogleAuthProvider();
    this.authFinished = false;

    // Set Firebase login Google provider data access scopes.
    this.provider.addScope('profile');
    this.provider.addScope('email');

    this.cordovaGooglePlusLoginOptions = {
      'webClientId': environment.cordovaGooglePlus.webClientId, // Required on Android, to get idToken. Optional on iOS.
    };

    this.cordovaGooglePlus = (window as any).plugins ? (window as any).plugins.googleplus : null;
    this.isCordova = typeof cordova != 'undefined';

    this.authReturnPromise = new Promise((resolve, reject) => {
      this.authReturnPromiseResolve = resolve;
    });

    // Handle state changes and silent login
    if (!this.isCordova) {
      firebase.auth().onAuthStateChanged(userData => {
        this.onAuth({ user: userData });
      });
    } else {
      this.cordovaGooglePlus.trySilentLogin(
        this.cordovaGooglePlusLoginOptions,
        authData => { this.onAuth(authData); },
        msg => { console.error('Authentication error: ' + msg); })
    }

  }

  login() {

    if (this.isCordova) { // Cordova authentication:
      //See: https://github.com/EddyVerbruggen/cordova-plugin-googleplus#usage
      this.cordovaGooglePlus.login(
        this.cordovaGooglePlusLoginOptions,
        authData => { this.onAuth(authData); },
        msg => { console.error('Authentication error: ' + msg); }
      );
    }
    else { // Firebase authentication:
      firebase.auth().signInWithPopup(this.provider).then(authData => {
        this.onAuth(authData);
      });
    }

    return this.authReturnPromise;
  }

  private onAuth(authData) {
    this.user = this.authDataToUser(this.isCordova ? 'cordova' : 'firebase', authData);
    this.authFinished = true;

    console.log('onAuth: authData', authData);
    console.log('onAuth: user', this.user);

    if (authData.user)
      this.writeUserToDB(this.user);

    this.authReturnPromiseResolve();
  }

  private authDataToUser(service: string, authData) {

    if (authData == null) return null;

    if (service == 'firebase') {
      let userData = authData.user;
      if (userData == null) return null;
      return new User('google', 'google:' + userData.uid, userData.displayName, userData.providerData[0].photoURL);
    }

    if (service == 'cordova')
      return new User('google', 'google:' + authData.userId, authData.displayName, authData.imageUrl);

    return null;
  }

  private writeUserToDB(user: User) {
    this.db.database.ref('users').child(user.id).set(user);
  }

  logout() {
    this.authFinished = false;
    if (this.isCordova) {
      this.cordovaGooglePlus.logout(function () { });
    } else {
      firebase.auth().signOut();
    }
    this.user = null;
  }
}
