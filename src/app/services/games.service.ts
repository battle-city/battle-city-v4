import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import * as firebase from 'firebase';
import * as _ from "lodash";
import * as moment from 'moment';

@Injectable()
export class GamesService {

  private db: AngularFireDatabase;
  public gamesRef: firebase.database.Reference;

  constructor(
    db: AngularFireDatabase,
  ) {
    this.db = db;
    this.gamesRef = this.db.database.ref('games');
  }

  getGamesRef() {
    return this.gamesRef;
  }

  getGameRef(gameId: string) {
    return this.gamesRef.child(gameId);
  }

  createGame(matchId: string, players: any[], number: number, scores: any[]) {

    // Initialize scores and convert from array to map.
    let scoresMap = {};
    _.forEach(scores, (score) => {
      score.current = score.initial;
      scoresMap[score.id] = score;
    });

    // Add scores to players
    _.forEach(players, (player) => {
      player.scores = _.cloneDeep(scoresMap);
    });

    var game = {
      matchId: matchId,
      players: players,
      number: number,
      state: 'inProgress',
    };

    let newGameRef = this.gamesRef.push(game);

    newGameRef.update({id: newGameRef.key})

    this.db.database.ref('matches').child(matchId).child('games').child(newGameRef.key).set({id: newGameRef.key, number: number});

    return newGameRef;
  }

  logEvent(gameId:string, eventType: string, actionBy:string, message: string) {
    var event = {
      eventType: eventType,
      actionBy: actionBy,
      message: message,
      timestamp: moment().format(),
    };
    console.log('logging event', event);
    this.gamesRef.child(gameId).child('log').push(event);
  }

}
