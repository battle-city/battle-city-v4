import { User } from './user';

export class Player {

    team: number;
    userId: string;
    name: string;
    profileImageURL: string;

    constructor(teamId: number, user: User) {
        this.team = teamId;
        this.userId = user.id,
        this.name = user.displayName,
        this.profileImageURL = user.profileImageURL;
    }
}
