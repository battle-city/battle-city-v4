export class Score {
    label: string;
    initial: number;
    lose: number;
    step: number;
    isPrimary?: boolean;
}
