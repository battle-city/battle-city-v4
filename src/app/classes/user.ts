export class User {

    provider: string;
    id: string;
    displayName: string;
    profileImageURL: string;

    constructor (provider, id, displayName, profileImageURL) {
        this.provider = provider;
        this.id = id;
        this.displayName = displayName;
        this.profileImageURL = profileImageURL;
    }
}
