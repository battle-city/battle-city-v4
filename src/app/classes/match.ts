import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import * as firebase from 'firebase';
import * as _ from "lodash";
import { Player } from '../classes/player';


export class Match {

    id: string;
    ref: firebase.database.Reference;
    exists: boolean;

    playersRef: firebase.database.Reference;
    configRef: firebase.database.Reference;
    scoresRef: firebase.database.Reference;
    gamesRef: firebase.database.Reference;

    players: any[];
    teams: any[]; // array of players

    state: string;
    config;
    games;

    newTeamIndex;

    constructor(firebaseId: string, db: AngularFireDatabase) {

        this.id = firebaseId;
        this.ref = db.database.ref('matches').child(this.id);

        this.playersRef = this.ref.child('players');
        this.configRef = this.ref.child('/config');
        this.scoresRef = this.configRef.child('scores');
        this.gamesRef = db.database.ref('games'); //this.GamesService.gamesRef;

        this.ref.once('value', (snapshot) => {
            this.exists = snapshot.exists();

            if (!this.exists) {
                console.error('Match \'' + this.id + '\' does not exist!')
                return;
            }

            this.playersRef.on('value', (snapshot) => {
                this.players = snapshot.val();
                this.sortPlayersIntoTeams();
                this.scrunchTeams();
            });

        });

    }

    addPlayer(user) {
        // These may be unnecessary, since we may already do this on DB update.
        this.sortPlayersIntoTeams();
        this.scrunchTeams();

        let player = new Player(this.teams.length, user);
        this.ref.child('players').child(user.id).set(player);
    }

    delete() {
        this.ref.remove();
    }

    sortPlayersIntoTeams() {
        this.teams = _.flow([_.groupBy, _.toArray])(this.players, 'team');
    }

    changeTeam(player, newTeamId: number) {
        player.team = newTeamId; // Conveniently, this also updates `this.teams`.
        this.sortPlayersIntoTeams();
        this.scrunchTeams();

        // We need to update all the players, since we scrunched the teams.
        this.updatePlayersInDb();
    }

    scrunchTeams() {
        _.forEach(this.teams, (team, teamId) => {
            _.forEach(team, (player) => {
                // console.log('set player to team', player.name, player.team, '->', teamId);
                player.team = teamId;
            });
        });
        this.updatePlayersInDb();
    }

    shuffleTeams(teamSize: number) {
        this.teams = _.chunk(_.shuffle(this.players), teamSize);
        this.scrunchTeams();
        this.updatePlayersInDb();
    }

    updatePlayersInDb() {
        if (this.players)
            this.playersRef.update(this.players);
    }

    removePlayer(playerToRemove) {

        // Delete and scrunch locally so that everyone gets put on the correct team post-delete.
        delete this.players[playerToRemove.userId];
        this.scrunchTeams();

        // Delete player remotely.
        this.playersRef.child(playerToRemove.userId).remove();
    }


}
