import { Score } from './score';

export class MatchConfig {
    gametypeId: string;
    variantId: string;
    scores: Score[];

    constructor(gametypeId, variantId, scores) {
        this.gametypeId = gametypeId;
        this.variantId = gametypeId;
        this.scores = scores;
     }
}
