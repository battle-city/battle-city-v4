import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
let npm_package = require('../../../package.json');


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  environment;
  versionNumber: number;
  constructor() { }

  ngOnInit() {
    this.environment = environment;
    this.versionNumber = npm_package.version;
  }

}
