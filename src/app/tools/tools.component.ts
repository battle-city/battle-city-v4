import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { MdDialog, MdDialogRef } from '@angular/material';
import { DiceModalComponent } from './dice-modal/dice-modal.component';

@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.css']
})
export class ToolsComponent implements OnInit {

  diceVals: number[];
  dice: any[];
  notes: string;

  constructor(
    public dialog: MdDialog,
  ) {
    this.diceVals = [4, 6, 8, 12, 20];
  }

  ngOnInit() {
  }

  promptRand(isCoin: boolean, maxVal: number) {
    var resultVal;
    if (isCoin) resultVal = _.random(0, 1) ? 'Heads' : 'Tails';
    else resultVal = _.random(1, maxVal);

    this.dialog.open(DiceModalComponent, {
      data: { isCoin: isCoin, resultVal: resultVal, maxVal: maxVal }
    }).afterClosed().subscribe(result => {
      if (result) this.promptRand(isCoin, maxVal);
    });

  }

  confirmNotesClear() {
    if (confirm('Clear notes?')) this.notes = '';
  }

}
