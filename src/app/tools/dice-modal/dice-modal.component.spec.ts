import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiceModalComponent } from './dice-modal.component';

describe('DiceModalComponent', () => {
  let component: DiceModalComponent;
  let fixture: ComponentFixture<DiceModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiceModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiceModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
