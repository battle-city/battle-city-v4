import { Component, OnInit, Inject } from '@angular/core';
import { MD_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-dice-modal',
  templateUrl: './dice-modal.component.html',
  styleUrls: ['./dice-modal.component.css']
})
export class DiceModalComponent implements OnInit {

  constructor(
    @Inject(MD_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit() {
  }

}
