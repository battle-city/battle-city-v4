import { Routes } from '@angular/router';

import { HomeComponent } from './home';
import { MatchComponent } from './match';
import { LoginComponent } from './login';
import { AboutComponent } from './about';

export const ROUTES: Routes = [
  { path: '',      component: HomeComponent },
  { path: 'home',  component: HomeComponent },
  { path: 'login',  component: LoginComponent },
  { path: 'about',  component: AboutComponent },
];