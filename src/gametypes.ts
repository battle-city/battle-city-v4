export const GAMETYPES = {
    mtg: {
        id: 'mtg',
        bestOf: 3,
        name: 'Magic: The Gathering',
        minPlayers: 2,
        maxPlayers: null,
        minTeams: 2,
        maxTeams: null,
        scores: {
            life: {
                id: 'life',
                label: 'Life',
                initial: '20',
                lose: 0,
                step: 1,
                isPrimary: true,
            },
            poison: {
                id: 'poison',
                label: 'Poison',
                initial: '0',
                lose: 10,
                step: 1,
            }
        },
        variants: {
            standard: {
                id: 'standard',
                name: 'Standard',
            },
            twoHeadedGiant: {
                id: 'twoHeadedGiant',
                name: 'Two-Headed Giant',
                scores: {
                    life: {
                        initial: '40',
                    },
                    poison: {
                        initial: '0',
                        lose: 20,
                    }
                }
            },
            commander: {
                id: 'commander',
                name: 'Commander',
                scores: {
                    life: {
                        initial: '40',
                    },
                }
            },
        }
    },
    ygo: {
        id: 'ygo',
        bestOf: 3,
        name: 'Yu-Gi-Oh!',
        minPlayers: 2,
        maxPlayers: null,
        minTeams: 2,
        maxTeams: null,
        scores: {
            life: {
                id: 'life',
                label: 'Life Points',
                initial: '8000',
                lose: 0,
                step: 100,
                isPrimary: true,
            },
        },
    }
};