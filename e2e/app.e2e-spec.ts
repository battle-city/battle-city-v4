import { BattleCityPage } from './app.po';

describe('battle-city App', () => {
  let page: BattleCityPage;

  beforeEach(() => {
    page = new BattleCityPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
